var MediaObject = (function()
{
	var tmpl = $([
		'<div class="media">',
			'<div class="media-left">',
				'<a href="#">',
					'<img class="media-object" src="" alt="">',
				'</a>',
			'</div>',
			'<div class="media-body">',
				'<h4 class="media-heading"></h4>',
			'</div>',
		'</div>'
		].join(""));

	this.showMediaLeft = function(sp)
	{
		tmpl.find('.media-left').show(sp || 300);
		return this;
	};

	this.hideMediaLeft = function(sp)
	{
		tmpl.find('.media-left').hide(sp || 300);
		return this;
	};

	this.setMediaLeftHtml = function(v)
	{
		tmpl.find('.media-left').html(v);
		return this;
	};

	this.setHeadingText = function(v)
	{
		tmpl.find('.media-heading').text(v);
		return this;
	};

	this.appendToHeading = function(v)
	{
		tmpl.find('.media-heading').append(v);
		return this;
	};

	this.appendToBody = function(v)
	{
		tmpl.find('.media-body').append(v);
		return this;
	};

	this.append = function(v)
	{
		tmpl.append(v);
		return this;
	};

	this.getHeading = function()
	{
		return tmpl.find('.media-heading');
	};

	this.getBody = function()
	{
		return tmpl.find('.media-body');
	};

	this.getTmpl = function()
	{
		return tmpl;
	};

	return this;
});