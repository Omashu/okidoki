var UploadImage = (function(FileObject, Config)
{
	// configure
	Config = $.extend(true, {
			formData : {
				"_token" : _Token
			},
			dataType : "json",
			previewCrop : true,
			previewMaxWidth : 64,
			previewMaxHeight : 64,
			maxFileSize : 10485760, // 10 МБ
			multiple : false,
			url : AjaxUploadImage
		}, Config);

	FileObject = $(FileObject);
	FileObject.prop("multiple", !!Config.multiple);

	var Self = this;
	var Callbacks = {
		"add" : $.Callbacks(),
		"progress" : $.Callbacks(),
		"done" : $.Callbacks(),
		"fail" : $.Callbacks(),
		"processAlways" : $.Callbacks()
	};

	var HandlerAdd = function(e, data)
	{
		$.each(data.files, function(index, file) {
			data.context = new (function()
			{
				var oStorage = {};
				var data = {loaded:0,total:1,res:null};

				this.storage = function(k,v)
				{
					if (typeof v !== "undefined")
					{
						oStorage[k] = v;
						return this;
					}

					return oStorage[k] || null;
				};

				this.getName = function()
				{
					return file.name;
				};

				this.getPreview = function()
				{
					return file.preview;
				};

				this.getPercent = function()
				{
					return parseInt(data.loaded / data.total * 100, 10);
				};

				this.getRes = function()
				{
					return data.res;
				};

				this.set = function(k,v)
				{
					data[k] = v;
					return this;
				};

				return this;
			});

			Callbacks.add.fire(data.context);
		});
	};

	var HandlerDone = function(e, data)
	{
		data.context.set("res", data.result);
		Callbacks.done.fire(data.context);
	};

	var HandlerProcessAlways = function(e, data)
	{
		Callbacks.processAlways.fire(data.context);
	};

	var HandlerFail = function(e, data)
	{
		Callbacks.fail.fire(data.context);
	};

	var HandlerProgress = function(e, data)
	{
		data.context.set("loaded", data.loaded);
		data.context.set("total", data.total);
		Callbacks.progress.fire(data.context);
	};

	this.on = function(name, func)
	{
		if (!Callbacks[name]) {
			return this;
		}

		Callbacks[name].add(func);
		return this;
	};

	// binding
	FileObject.fileupload(Config).on("fileuploadadd", HandlerAdd)
		.on("fileuploaddone", HandlerDone)
		.on("fileuploadprocessalways", HandlerProcessAlways)
		.on("fileuploadfail", HandlerFail)
		.on("fileuploadprogress", HandlerProgress);

	return this;
});