var Publisher = (function(Id)
{
	var Steps = {
		"selectingCategories" : {
			"state" : "Loading...",
			"message" : "Выбираем категории..."
		},
		"generalInfo" : {
			"state" : "Loading...",
			"message" : "Заполняем основную информацию объявления..."
		},
		"images" : {
			"state" : "Loading...",
			"message" : "Загружаем изображения..."
		},
		"paidServices" : {
			"state" : "Loading...",
			"message" : "Настраиваем платные сервисы..."
		},
		"publish" : {
			"state" : "Loading...",
			"message" : "Публикуем..."
		}

	};

	var ResRowTmpl = [
		'<div><span class="js-state">Loading...</span> <span class="js-message"></span> <div class="js-response"></div></div>'
	].join("");

	var ResBlocks = [];
	var Callbacks = {
		"before" : $.Callbacks(),
		"after" : $.Callbacks()
	};
	var Self = this;
	var Order = [];
	var BExec = false;

	this.addResultsBlock = function(obj)
	{
		ResBlocks.push($(obj));
		return this;
	};

	this.on = function(name, callback)
	{
		if (!Callbacks[name]) {
			console.log("Undefined callback", name);
			return this;
		}

		Callbacks[name].add(callback);
		return this;
	};

	this.exec = function()
	{
		if (BExec) {
			console.log("Executing is already...")
			return this;
		}

		BExec = true;
		Callbacks.before.fire();
		Order = ["selectingCategories", "generalInfo", "images", /*"paidServices",*/ "publish"];
		Exec(0);
		return this;
	};

	var Captcha = function(asset, callback)
	{
		var CapBlock = $([
			'<div class="panel panel-primary">',
				'<div class="panel-heading">Введите капчу</div>',
				'<div class="panel-body"></div>',
				'<div class="panel-footer"></div>',
			'</div>'
		].join(""));

		// показываем капчу
		ResBlocks[0].append(CapBlock);

		var btn = $('<button/>').text("Отправить").prop("type", "button").addClass("btn btn-primary");
		var input = $('<input/>').addClass("form-control").prop("type", "text");

		CapBlock.find(".panel-body").append($('<img/>').addClass("thumbnail").prop("src", asset));
		CapBlock.find(".panel-body").append(input);
		CapBlock.find(".panel-footer").append(btn);

		btn.click(function()
		{
			callback(input.val());
			CapBlock.remove();
		});
	};

	var Exec = function(key, appends)
	{
		var stepKey = Order[key];
		appends = appends || {};

		// генерируем шаблон
		var obj = $(ResRowTmpl).addClass("alert alert-warning");
		obj.find('.js-state').text(Steps[stepKey].state);
		obj.find('.js-message').text(Steps[stepKey].message);

		// вставляем в контент
		ResBlocks[0].append(obj);

		// выполняем запрос
		Query(stepKey, appends, function(e)
		{
			// вставляем в шаблон ответы
			obj.find('.js-state').html(e.state);
			obj.find('.js-response').html(e.message);

			if (!e.bOk) {
				if (e.code === 100)
				{
					// показать капчу
					Captcha(e.data.captchaImageAsset, function(value)
						{
							// выполним повторно запрос, передадим введенное значение капчи
							Exec(key, {"captcha":value, "displaycode" : e.data.displaycode});
						});
					return;
				}

				// ошибка
				BExec = false;
				obj.removeClass("alert-warning").addClass("alert-danger");
				return;
			}

			// шаг выполнен, переходим к следующему
			obj.removeClass("alert-warning").addClass("alert-success");
			var nextKey = ++key;
			if (nextKey > Order.length-1) {
				BExec = false;
			} else {
				Exec(nextKey);
			}
		}, function(e)
		{
			BExec = false;
			obj.removeClass("alert-warning").addClass("alert-danger");

			obj.find('.js-state').html("<b>Ошибка!</b>");
			obj.find('.js-message').html("На шаге "+stepKey+" произошла внутренняя ошибка.");
		});
	};

	var Query = function(q, appends, done, fail)
	{
		var params = appends;
		params.id = Id;
		params.type = q;

		var ajax = $.ajax({
			headers: {
				'X-CSRF-TOKEN': _Token
			},
			url: AjaxAdvertisementPublishUrl,
			type: 'POST',
			dataType: 'json',
			data: params,
		})
		.done(done)
		.fail(fail);
	};

	return this;
});