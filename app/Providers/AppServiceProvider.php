<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
		$app = $this->app;
		$this->app->validator->extend('advert_login', function($attribute, $value, $parameters) use ($app)
		{
			return in_array($value, array_keys($app->config->get("okidoki.logins")), true);
		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);

		$this->app->singleton("OkiAuth", function($app)
		{
			return new \App\OkiAuth($app->config->get("okidoki.logins"), $app->cache);
		});
	}

}
