<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Requests;

class Advert extends Model {

	const E_CAPTHA = 100;

	protected $fillable = 
	[
		'login',
		'category_id',
		'adtype',
		'title_ee',
		'title_ru',
		'subtitle_ee',
		'subtitle_ru',
		'price_type',
		'price',
		'best_offer',
		'tax',
		'amount_sel',
		'amount_val',
		'deliver_self',
		'deliver_smartpost',
		'deliver_post24',
		'deliver_post',
		'deliver_dpd',
		'deliver_other',
		'pay_okidoki',
		'pay_cash',
		'pay_bank',
		'pay_seedetails',
		
		'returns',
		'returns_days',
		'returns_how',
		'returns_who_pays',
		'returns_comments',

		'desc_ee',
		'desc_ru',
	];

	protected $guarded = ['*'];
	protected $hidden = ["id", "okidoki_id", "published_at", "created_at", "updated_at"];

	public function __construct(array $attributes = [])
	{
		// defaults
		$this->setRawAttributes(
			[
				"adtype" => 1,
				"price_type" => 1,
				"price" => 0.00,
				"best_offer" => 1,
				"tax" => 0,
				"amount_sel" => 1,
				"amount_val" => 1,
				'deliver_self' => 0,
				'deliver_smartpost' => 0,
				'deliver_post24' => 0,
				'deliver_post' => 0,
				'deliver_dpd' => 0,
				'deliver_other' => 0,
				'pay_okidoki' => 0,
				'pay_cash' => 0,
				'pay_bank' => 0,
				'pay_seedetails' => 0,
				'returns' => 0,
				'returns_days' => 14,
				'returns_how' => 1,
				'returns_who_pays' => 1,
			], true);

		parent::__construct($attributes);
	}

	public function category()
	{
		return $this->hasOne('\App\Category', 'okidoki_id', 'category_id');
	}

	// прикрепленные изображения
	public function images()
	{
		return $this->morphToMany('\App\Image', 'imageable');
	}

	public function syncImagesFromArray($images=null)
	{
		$checkedImageIds = [];
		if (is_array($images) AND $images)
		{
			foreach ($images as $imageId)
			{
				if (!is_string($imageId) AND !is_numeric($imageId) AND !(int)$imageId)
					continue;

				$checkedImageIds[] = (int) $imageId;
			}
		}

		$checkedImageIds = array_unique($checkedImageIds);

		if ($checkedImageIds)
			$checkedImageIds = Image::whereIn("id", $checkedImageIds)->get()->modelKeys();

		// сцепляем/отцепляем
		$this->images()->sync($checkedImageIds);
	}

	public function pushSelectingCategories()
	{
		if (!$this->category_id OR !$this->category)
			throw new AdvertPushException("Неизвестная категория, отредактируйте объявление.");

		$OkiAuth = \App::make("OkiAuth");
		if (!$OkiAuth->check($this->login))
			$OkiAuth->authorize($this->login);

		// создаем объявление
		$values = ["item" => (int) $this->okidoki_id, "p" => 1, "cat_id" => $this->category_id];
		$request = Requests::post("http://www.okidoki.ee/ru/sell/category/", $OkiAuth->makeHeaders($this->login), $values);

		if (count($request->history))
		{
			$history = $request->history[0];

			// при первой публикации будет там
			preg_match("/.*=(?<id>\d+)$/", $history->url, $mathes);
			if (!$mathes OR !(int)$mathes["id"])
			{
				// при обновлении будет тут
				preg_match("/.*=(?<id>\d+)$/", $history->headers["location"], $mathes);

				if (!$mathes OR !(int)$mathes["id"])
				{
					// ID ранее небыло, значит неудалось создать объявление
					if (!$this->okidoki_id)
						throw new AdvertPushException("Неудалось получить идентификатор объявления");

					// ID ранее был, вероятно объявление удалено, попробуем еще раз без ID
					$this->okidoki_id = 0;
					$this->save();
					return $this->pushSelectingCategories();
				}
			}

			$this->okidoki_id = (int)$mathes["id"];
			$this->save();

			// категория выбрана объявление создано/обновлено
			return $this;
		}

		// переадресации небыло, значит какая-то ошибка, распарсим...
		preg_match("/div.*class=\"content\">.*<p.*class=\"error\">(?<error>.*)<\/p/Us", $request->body, $mathes);

		if ($mathes)
			throw new AdvertPushException(htmlentities($mathes["error"]));

		return $this;
	}

	public function pushGeneralInfo()
	{
		if (!$this->okidoki_id)
			throw new AdvertPushException("Первый шаг не выполнен, объявление не создано.");

		$OkiAuth = \App::make("OkiAuth");
		if (!$OkiAuth->check($this->login))
			$OkiAuth->authorize($this->login);

		$values =
		[
			"item" => (int) $this->okidoki_id,
			"p" => 1,
			"adtype" => $this->adtype,
			"title_ee" => $this->title_ee,
			"title_ru" => $this->title_ru,
			"subtitle_ee" => $this->subtitle_ee,
			"subtitle_ru" => $this->subtitle_ru,
			"objecttype" => $this->price_type,
			"price" => $this->price,
			"bestoffer" => $this->best_offer,
			"vat" => $this->tax,
			"amount1" => $this->amount_sel,
			"amount2" => $this->amount_val,

			"returns" => $this->returns,
			"returns_days" => $this->returns_days,
			"returns_how" => $this->returns_how,
			"returns_who_pays" => $this->returns_who_pays,
			"returns_comments" => $this->returns_comments,

			"country" => "EE",
			"city_id" => 1,
			"city" => "Tallinn",

			"desc_ee" => $this->desc_ee,
			"desc_ru" => $this->desc_ru,
		];

		if ($this->deliver_self)
			$values["deliver_self"] = "on";

		if ($this->deliver_smartpost)
			$values["deliver_smartpost"] = "on";

		if ($this->deliver_post24)
			$values["deliver_post24"] = "on";

		if ($this->deliver_post)
			$values["deliver_post"] = "on";

		if ($this->deliver_dpd)
			$values["deliver_dpd"] = "on";

		if ($this->deliver_other)
			$values["deliver_other"] = "on";

		if ($this->pay_okidoki)
			$values["pay_okidoki"] = "on";

		if ($this->pay_cash)
			$values["pay_cash"] = "on";

		if ($this->pay_bank)
			$values["pay_bank"] = "on";

		if ($this->pay_seedetails)
			$values["pay_seedetails"] = "on";

		$url = "http://www.okidoki.ee/ru/sell/general/?".http_build_query(["item"=>$this->okidoki_id]);
		$request = Requests::post($url, $OkiAuth->makeHeaders($this->login), $values);

		if (count($request->history))
			return $this;

		// переадресации небыло, значит какая-то ошибка, смотрим список
		preg_match("/div.*class=\"content\">.*<ul\sclass=\"error\".*>(?<error>.*)<\/ul/Us", $request->body, $mathes);

		if ($mathes) {
			preg_match_all("/<li>(?<error>.*)<\/li>/", $mathes["error"], $mathes);
			throw new AdvertPushException($mathes ? htmlentities(implode(", ", $mathes["error"])) : "Введите корректные данные...");
		}

		// либо 1 ошибка
		preg_match("/div.*class=\"content\">.*<p.*class=\"error\">(?<error>.*)<\/p/Us", $request->body, $mathes);

		if ($mathes)
			throw new AdvertPushException(htmlentities($mathes["error"]));

		return $this;
	}

	public function pushImages()
	{
		if (!$this->okidoki_id)
			throw new AdvertPushException("Первый шаг не выполнен, объявление не создано.");

		// изображений нет
		if (!$this->images->count())
			return $this;

		$url = "http://www.okidoki.ee/ru/sell/images/?".http_build_query(["item"=>$this->okidoki_id]);

		$OkiAuth = \App::make("OkiAuth");
		if (!$OkiAuth->check($this->login))
			$OkiAuth->authorize($this->login);

		// выполним удаление загруженных изображений, если они есть
		$request = Requests::get($url, $OkiAuth->makeHeaders($this->login), null);
		preg_match("/id=\"cphotos\"(?<images>.*)<div\sid=\"uploadfiles\"/Us", $request->body, $mathes);
		if ($mathes)
		{
			preg_match_all("/p_id=(?<id>\d+)/", $mathes["images"], $mathes);

			if ($mathes)
			{
				$ids = array_unique($mathes["id"]);
				foreach ($ids as $id)
					Requests::get("http://www.okidoki.ee/ru/sell/images/?".http_build_query(["item"=>$this->okidoki_id,"p"=>1,"p_id"=>$id]), $OkiAuth->makeHeaders($this->login), null);
			}
		}

		$values = ["item" => $this->okidoki_id, "p" => 4];
		$i = 1;
		foreach ($this->images as $image)
			$values["upfile_".$i++] = new \CurlFile(public_path($image->getRelPath()), $image->mime_type, $image->filename.".".$image->format);

		$request = Requests::post($url, $OkiAuth->makeHeaders($this->login), $values,
			[
				"transport" => "\App\Requests_Transport_cURL",
			]);
		return $this;
	}

	protected function checkCaptcha($force=false)
	{
		if (\Request::get("displaycode") AND \Request::get("captcha") AND $force === false)
			return \Request::only("displaycode", "captcha");

		$OkiAuth = \App::make("OkiAuth");
		if (!$OkiAuth->check($this->login))
			$OkiAuth->authorize($this->login);

		$url = "http://www.okidoki.ee/ru/sell/preview/?".http_build_query(["item" => $this->okidoki_id]);
		$request = Requests::get($url, $OkiAuth->makeHeaders($this->login), null);

		// капчи нет, можно добавить без нее
		if (!preg_match("/id=\"capimg\"/", $request->body))
			return [];

		preg_match("/displaycode\"\svalue=\"(?<code>[a-z0-9A-Z]+)\"/", $request->body, $mathes);

		// неудалось спарсить соль капчи
		if (!$mathes)
			throw new AdvertPushException("Неудалось получить дополнительный ключ капчи.");

		// парсим капчу
		$captchaUrl = "http://www.okidoki.ee/captcha/?".$mathes["code"]."&a&".http_build_query(["u" => time()]);
		$request = Requests::get($captchaUrl, $OkiAuth->makeHeaders($this->login), null);

		$filename = time() . ".jpg";
		$storagePath = "/captcha/" . $filename;
		$assetUrl = asset("captcha/".$filename);

		$disk = \Storage::disk('localPublic');
		$disk->put($storagePath, $request->body);

		throw new AdvertPushException("Введите код с картинки...", self::E_CAPTHA,
			[
				"displaycode" => $mathes["code"],
				"captchaImageAsset" => $assetUrl,
			]);
	}

	// displaycode = (ключ полученный в пред. запросе), captcha = введенная капча
	public function pushPublish()
	{
		if (!$this->okidoki_id)
			throw new AdvertPushException("Первый шаг не выполнен, объявление не создано.");

		$OkiAuth = \App::make("OkiAuth");
		if (!$OkiAuth->check($this->login))
			$OkiAuth->authorize($this->login);

		$values = $this->checkCaptcha();

		// дальше не пускаем, если это локальные тесты
		if (env('APP_ENV') == "local")
		{
			$this->published_at = date("Y-m-d H:i:s");
			$this->save();

			throw new AdvertPushException("Объявление помечено опубликованным, но по факту опубликовано небыло.");
		}

		$values["item"] = $this->okidoki_id;

		$url = "http://www.okidoki.ee/ru/sell/done/?".http_build_query($values);
		$request = Requests::get($url, $OkiAuth->makeHeaders($this->login), null);

		preg_match("/div.*class=\"content\">.*<p.*class=\"error\">(?<error>.*)<\/p/Us", $request->body, $mathes);

		// ошибка публикации
		if ($mathes) {
			if (preg_match("/код/u", $mathes["error"])) $this->checkCaptcha(true);
			throw new AdvertPushException(htmlentities($mathes["error"]));
		}

		$this->published_at = date("Y-m-d H:i:s");
		$this->save();
		return $this;
	}
}
