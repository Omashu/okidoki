<?php namespace App;

class AdvertPushException extends \Exception {

	protected $data;

	public function __construct($message, $code = 1, array $data = [])
	{
		$this->message = $message;
		$this->code = $code;
		$this->data = $data;
	}

	public function getData()
	{
		return $this->data;
	}
}