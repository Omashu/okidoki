<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

	protected static $uploadLastValidator = NULL;

	public function adverts()
	{
		return $this->morphedByMany('\App\Advert', 'imageable');
	}

	public static function getValidator()
	{
		return static::$uploadLastValidator;
	}

	public static function upload(\Symfony\Component\HttpFoundation\File\UploadedFile $file)
	{
		$mainRelPath = "/uploads/images/";
		$mainAbsPath = public_path() . $mainRelPath;

		$validator = \Validator::make(["file" => $file], ["file" => "required|image|max:5000|mimes:png,gif,jpeg,jpg"]);

		if ($validator->fails())
		{
			static::$uploadLastValidator = $validator;
			return FALSE;
		}

		$ext = $file->getClientOriginalExtension();

		$filename = preg_replace("/\.{$ext}$/", "", $file->getClientOriginalName());

		// чистим название
		$title = $filename;
		$title = str_replace("-", " ", $title);
		$title = str_replace("_", " ", $title);
		$title = preg_replace("/\s\d+$/", "", $title);
		$title = preg_replace("/^\d+\s/", "", $title);
		$title = trim($title);

		$filename = str_random(8);

		// из хеша получим директории хранения
		$filenameHash = md5($filename);

		$folders = [substr($filenameHash, 0, 2), substr($filenameHash, 2, 2)];

		$fullAbsPath = $mainAbsPath . implode("/", $folders) . "/";
		$fullRelPath = $mainRelPath . implode("/", $folders) . "/";

		do {
			$filename = str_random(2) . $filename;
		} while(file_exists($fullAbsPath . $filename . "." . $ext));

		// создаем сущность
		$image = new Image();
		$image->title = $title;
		$image->filename = $filename;
		$image->mime_type = $file->getMimeType();
		$image->format = $ext;

		// переносим файл
		$file->move($fullAbsPath, $filename . "." . $ext);

		$fileAbsPath = $fullAbsPath . $filename . "." . $ext;
		$fileRelPath = $fullRelPath . $filename . "." . $ext;

		$image->path = $fullRelPath;

		// получаем доп. инфу
		list($width, $height) = @getimagesize($fileAbsPath);
		if (!$width) $width = 0;
		if (!$height) $height = 0;

		// готово ;)
		$image->bytes = filesize($fileAbsPath);
		$image->width = $width;
		$image->height = $height;
		$image->save();

		return $image;
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['title'] = htmlentities($value);
	}

	public function getHtmlLink($href = "#", $title = null, array $attrs = [], $entities = false)
	{
		$url = url($href, [], null);
		if (is_null($title) || $title === false) $title = $url;

		return '<a href="'.$url.'"'.HTML::attributes($attrs).'>'. ($entities ? $this->entities($title) : $title) .'</a>';
	}

	public function getHtmlImage($src, $alt = null, array $attrs = [])
	{
		return HTML::image($src, $alt, $attrs);
	}

	public function getRelPath()
	{
		return $this->path . $this->filename . "." . $this->format;
	}

	public function getUrl()
	{
		return asset($this->getRelPath());
	}
}
