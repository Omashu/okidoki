<?php namespace App;

use Requests_Transport_cURL AS Default_Requests_Transport_cURL;
use Requests;

/**
 * Наследуем и изменим медот для фикса отправки файлов
 * CurlFile, pull request создан, однако по тестам не прошел, будем ждать офф. решения от разработчиков, а пока как-то так...
 */

class Requests_Transport_cURL extends Default_Requests_Transport_cURL
{
	/**
	 * Setup the cURL handle for the given data
	 *
	 * @param string $url URL to request
	 * @param array $headers Associative array of request headers
	 * @param string|array $data Data to send either as the POST body, or as parameters in the URL for a GET/HEAD
	 * @param array $options Request options, see {@see Requests::response()} for documentation
	 */
	protected function setup_handle($url, $headers, $data, $options) {
		$options['hooks']->dispatch('curl.before_request', array(&$this->fp));

		$headers = Requests::flatten($headers);
		if (in_array($options['type'], array(Requests::HEAD, Requests::GET, Requests::DELETE)) & !empty($data)) {
			$url = self::format_get($url, $data);
		}
		elseif (!empty($data) && !is_string($data)) {
			// комментируем конфертацию данных
			// $data = http_build_query($data, null, '&');
		}

		switch ($options['type']) {
			case Requests::POST:
				curl_setopt($this->fp, CURLOPT_POST, true);
				curl_setopt($this->fp, CURLOPT_POSTFIELDS, $data);
				break;
			case Requests::PATCH:
			case Requests::PUT:
				curl_setopt($this->fp, CURLOPT_CUSTOMREQUEST, $options['type']);
				curl_setopt($this->fp, CURLOPT_POSTFIELDS, $data);
				break;
			case Requests::DELETE:
				curl_setopt($this->fp, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			case Requests::HEAD:
				curl_setopt($this->fp, CURLOPT_NOBODY, true);
				break;
		}

		curl_setopt($this->fp, CURLOPT_URL, $url);
		curl_setopt($this->fp, CURLOPT_TIMEOUT, $options['timeout']);
		curl_setopt($this->fp, CURLOPT_CONNECTTIMEOUT, $options['timeout']);
		curl_setopt($this->fp, CURLOPT_REFERER, $url);
		curl_setopt($this->fp, CURLOPT_USERAGENT, $options['useragent']);
		curl_setopt($this->fp, CURLOPT_HTTPHEADER, $headers);

		if (true === $options['blocking']) {
			curl_setopt($this->fp, CURLOPT_HEADERFUNCTION, array(&$this, 'stream_headers'));
		}
	}
}