<?php namespace App;

use Requests;

class OkiAuth {

	protected $logins;
	protected $cache;

	public function __construct($logins, \Illuminate\Cache\CacheManager $cache)
	{
		$makeLogins = [];
		foreach ($logins as $login => $password) {
			$makeLogins[$login] = [
				"authorized" => false,
				"password" => $password,
				"cookies" => null,
			];
		}

		$this->logins = $makeLogins;
		$this->cache = $cache;
	}

	public function check($login)
	{
		if (!isset($this->logins[$login]["authorized"]))
			throw new OkiAuthException("Такого логина не существует");

		// уже авторизован
		if ($this->logins[$login]["authorized"])
			return true;

		// подтянем куки, если есть
		if (!$this->logins[$login]["cookies"])
			$this->logins[$login]["cookies"] = $this->cache->get("OkiAuth_Cache_".$login);

		// check is authorized
		$request = Requests::get("http://www.okidoki.ee/", $this->makeHeaders($login), null);
		$this->logins[$login]["authorized"] = (bool) preg_match("/id=\"auth\".*logout.*<\/li>/Us", $request->body);
		return $this->logins[$login]["authorized"];
	}

	public function authorize($login)
	{
		if (!isset($this->logins[$login]["authorized"]))
			throw new OkiAuthException("Такого логина не существует");

		if ($this->logins[$login]["authorized"])
			return true;

		$request = Requests::post("https://www.okidoki.ee/ru/login/try/", [], 
			[
				"ref" => "http://www.okidoki.ee/ru/sell/",
				"r" => "sell/category",
				"login" => $login,
				"password" => $this->logins[$login]["password"]
			]);

		if (!count($request->history))
			throw new OkiAuthException("Неправильное имя пользователя и/или пароль");

		$this->logins[$login]["authorized"] = true;
		$this->logins[$login]["cookies"] = $request->cookies;
		$this->cache->forever("OkiAuth_Cache_".$login, $request->cookies);
		return true;
	}

	public function makeHeaders($login)
	{
		if (!isset($this->logins[$login]["authorized"]))
			return [];

		if (!$this->logins[$login]["cookies"])
			return [];

		$cookies = [];
		foreach ($this->logins[$login]["cookies"] as $cookie)
			$cookies[] = $cookie->formatForHeader();

		return ["Cookie" => implode("; ", $cookies)];
	}
}
