<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::pattern("id", "\d+");

Route::get('/', "AdvertisementController@index");
Route::get("advertisement", ["as" => "advertisement", "uses" => "AdvertisementController@index"]);

// создание
Route::get("advertisement/new", ["as" => "advertisementNew", "uses" => "AdvertisementController@getNew"]);
Route::post("advertisement/new", "AdvertisementController@postNew");

// редактирование не опубликованных объявлений
Route::get("advertisement/{id}/edit", ["as" => "advertisementEdit", "uses" => "AdvertisementController@edit"]);
Route::post("advertisement/{id}/edit", "AdvertisementController@postEdit");

// удалить объявление
Route::get("advertisement/{id}/delete", ["as" => "advertisementDelete", "uses" => "AdvertisementController@delete"]);
Route::post("advertisement/{id}/delete", "AdvertisementController@postDelete");

// список категорий
Route::get("categories", ["as" => "categories", "uses" => "CategoriesController@index"]);
Route::get("categories/sync", ["as" => "categoriesSync", "uses" => "CategoriesController@sync"]);

// публикация
Route::get("advertisement/{id}/publish", ["as" => "advertisementPublish", "uses" => "AdvertisementController@publish"]);
Route::post("ajax/advertisement/publish", ["as" => "ajaxAdvertisementPublish", "uses" => "AjaxAdvertisementPublishController@exec"]);

// клонирование и редактирование
Route::get("advertisement/{id}/cloneAndEdit", ["as" => "advertisementCloneAndEdit", "uses" => "AdvertisementController@cloneAndEdit"]);

// клонирование и публикация
Route::get("advertisement/{id}/cloneAndPublish", ["as" => "advertisementCloneAndPublish", "uses" => "AdvertisementController@cloneAndPublish"]);

// загрузка изображений
Route::post("ajax/upload/image", ["as" => "ajaxUploadImage", "uses" => "AjaxUploadController@image"]);
