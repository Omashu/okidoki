<?php namespace App\Http\Controllers;

use \App\Advert;
use \App\OkiAuthException;
use \App\AdvertPushException;

class AjaxAdvertisementPublishController extends Controller {

	/**
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('csrf');
	}

	/**
	 * @return Response
	 */
	public function exec()
	{
		$q = \Request::get("type");
		$advertId = \Request::get("id");
		$advert = Advert::findOrFail($advertId);

		if ($advert->published_at)
		{
			// уже опубликовано, блочим запросы
			return [
				"bOk" => false,
				"message" => "Это объявление уже опубликовано",
				"state" => "Ошибка!"
			];
		}

		switch ($q) {
			case 'selectingCategories':
				return $this->_selectingCategories($advert);
				break;
			case 'generalInfo':
				return $this->_generalInfo($advert);
				break;
			case 'images':
				return $this->_images($advert);
				break;
			case 'paidServices':
				return $this->_paidServices($advert);
				break;
			case 'publish':
				return $this->_publish($advert);
				break;
		}

		return [
			"bOk" => false,
			"message" => "Неизвестный запрос",
			"state" => "Ошибка!",
		];
	}

	protected function _selectingCategories(Advert $advert)
	{
		try {
			// проверит авторизацю
			// создаем объявление, выбирая категорию
			$advert->pushSelectingCategories();
		} catch(OkiAuthException $e) {
			return [
				"bOk" => false,
				"message" => "OkiAuthException: " . $e->getMessage(),
				"state" => "Ошибка авторизации!",
			];
		} catch(AdvertPushException $e) {
			return [
				"bOk" => false,
				"message" => "AdvertPushException: " . $e->getMessage(),
				"state" => "Ошибка публикации!",
			];
		}

		return [
			"bOk" => true,
			"message" => "Закреплено за категорией <b>".$advert->category->title."</b>. Объявлению присвоен #".$advert->okidoki_id.".",
			"state" => "Выполнено!",
		];
	}

	protected function _generalInfo(Advert $advert)
	{
		try {
			$advert->pushGeneralInfo();
		} catch(OkiAuthException $e) {
			return [
				"bOk" => false,
				"message" => "OkiAuthException: " . $e->getMessage(),
				"state" => "Ошибка авторизации!",
			];
		} catch(AdvertPushException $e) {
			return [
				"bOk" => false,
				"message" => "AdvertPushException: " . $e->getMessage(),
				"state" => "Ошибка публикации!",
			];
		}

		return [
			"bOk" => true,
			"message" => "Основная информация объявления отредактирована.",
			"state" => "Выполнено!",
		];
	}

	protected function _images(Advert $advert)
	{
		try {
			$advert->pushImages();
		} catch(OkiAuthException $e) {
			return [
				"bOk" => false,
				"message" => "OkiAuthException: " . $e->getMessage(),
				"state" => "Ошибка авторизации!",
			];
		} catch(AdvertPushException $e) {
			return [
				"bOk" => false,
				"message" => "AdvertPushException: " . $e->getMessage(),
				"state" => "Ошибка публикации!",
			];
		}

		return [
			"bOk" => true,
			"message" => "Изображения успешно обновлены.",
			"state" => "Выполнено!",
		];
	}

	protected function _paidServices(Advert $advert)
	{
		return [
			"bOk" => true,
			"message" => "Не реализовано.",
			"state" => "Пропуск!"
		];
	}

	protected function _publish(Advert $advert)
	{
		try {
			$advert->pushPublish();
		} catch(OkiAuthException $e) {
			return [
				"bOk" => false,
				"message" => "OkiAuthException: " . $e->getMessage(),
				"state" => "Ошибка авторизации!",
			];
		} catch(AdvertPushException $e) {
			return [
				"bOk" => false,
				"message" => "AdvertPushException: " . $e->getMessage(),
				"state" => "Ошибка публикации!",
				"code" => $e->getCode(),
				"data" => $e->getData(),
			];
		}

		return [
			"bOk" => true,
			"message" => "Объявление успешно опубликовано.",
			"state" => "Выполнено!",
		];
	}
}
