<?php namespace App\Http\Controllers;

use \App\Advert;

class AdvertisementController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('advertisement', ["adverts" => Advert::orderBy("created_at", "desc")->paginate(50)]);
	}

	public function getNew()
	{
		$logins = [];
		foreach (array_keys(\Config::get("okidoki.logins", [])) as $login)
			$logins[$login] = $login;

		return view('advertisement.new', ['logins' => $logins, "advert" => new Advert()]);
	}

	public function postNew(\App\Http\Requests\AdvertisementNewRequest $request)
	{
		$advert = new Advert($request->all());
		$advert->save();

		$advert->syncImagesFromArray($request->get("images", []));

		return redirect()->route('advertisementPublish', $advert)
			->with("successMessage", "Объявление успешно создано и доступно к публикации на okidoki.");
	}

	public function publish($id)
	{
		$advert = Advert::findOrfail($id);

		return view('advertisement.publish', ["advert" => $advert]);
	}

	public function delete($id)
	{
		$advert = Advert::findOrfail($id);

		return view('advertisement.delete', ["advert" => $advert]);
	}

	public function postDelete($id)
	{
		$advert = Advert::findOrfail($id);
		$advert->delete();

		return redirect()->route('advertisement')
			->with("successMessage", "Объявление ".$advert->title_ru." удалено.");
	}

	public function edit($id)
	{
		$advert = Advert::findOrfail($id);
		if ($advert->published_at)
			return redirect()->route('advertisement')
				->with("successMessage", "Объявление ".$advert->title_ru." уже опубликовано на сайте okidoki и почему-то я думаю, что изменять его в базе тоже не нужно...");

		$logins = [];
		foreach (array_keys(\Config::get("okidoki.logins", [])) as $login)
			$logins[$login] = $login;

		return view('advertisement.new', ["advert" => $advert, "logins" => $logins]);
	}

	public function postEdit(\App\Http\Requests\AdvertisementNewRequest $request, $id)
	{
		$advert = Advert::findOrfail($id);
		if ($advert->published_at)
			return redirect()->route('advertisement')
				->with("successMessage", "Объявление ".$advert->title_ru." уже опубликовано на сайте okidoki и почему-то я думаю, что изменять его в базе тоже не нужно...");

		$advert->update($request->all());
		$advert->syncImagesFromArray($request->get("images", []));

		return redirect()->route('advertisementPublish', $advert)
			->with("successMessage", "Объявление успешно обновлено и доступно к публикации на okidoki.");
	}

	public function cloneAndEdit($id)
	{
		$advert = Advert::findOrfail($id);
		$newAdvert = new Advert($advert->toArray());
		$newAdvert->save();

		$newAdvert->syncImagesFromArray($advert->images()->get()->modelKeys());
		return redirect()->route('advertisementEdit', $newAdvert);
	}

	public function cloneAndPublish($id)
	{
		$advert = Advert::findOrfail($id);
		$newAdvert = new Advert($advert->toArray());
		$newAdvert->save();

		$newAdvert->syncImagesFromArray($advert->images()->get()->modelKeys());
		return redirect()->route('advertisementPublish', $newAdvert);
	}
}
