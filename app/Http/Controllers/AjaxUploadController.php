<?php namespace App\Http\Controllers;

class AjaxUploadController extends Controller {

	public function image()
	{
		$file = \Request::file("image");
		$bOk = false;

		$aResults = [
			"bOk" => &$bOk,
			"message" => null,
			"file" => []
		];

		if (!$file)
		{
			$aResults["message"] = "Файл не передан";
			return $aResults;
		}

		$model = \App\Image::upload($file);
		if (!$model)
		{
			// error
			$aResults["message"] = "Файл не является изображением, либо его размеры слишком велики";
			return $aResults;
		}

		$bOk = true;
		$aResults["file"] =
		[
			"id" => $model->id,
			"title" => $model->title,
			"relUrl" => $model->getRelPath(),
			"absUrl" => $model->getUrl(),
		];

		$aResults["message"] = "Изображение загружено";
		return $aResults;
	}
}
