<?php namespace App\Http\Controllers;

class CategoriesController extends Controller {

	/**
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * @return Response
	 */
	public function index()
	{
		return view('categories', ["categories" => \App\Category::get()]);
	}

	public function sync()
	{
		$request = \Requests::get("http://www.okidoki.ee/ru/buy/");

		if ($request->status_code !== 200)
			return redirect()->route('categories')
				->with("errorMessage", "Произошла ошибка, вероятно сайт okidoki недоступен.");

		// удаляем категории, которые можем спарсить
		\DB::table("categories")->where("static", false)->delete();

		// общий блок категорий
		preg_match("/id=\"categories\">(?<categories>.*)<\/div>/Us", $request->body, $matches);

		if (!$matches)
			return redirect()->route('categories')->with("errorMessage", "Произошла ошибка, вероятно изменилась верстка на сайте okidoki, необходимо обновление скрипта.");

		$html = $matches["categories"];
		preg_match_all("/<dl>(?<blocks>.*)<\/dl>/Us", $html, $matches);

		$blocks = isset($matches["blocks"]) ? $matches["blocks"] : [];
		$categories = [];

		foreach ($blocks as $block)
		{
			preg_match_all("/<a.*href=\".*\/(?<id>[^\/]+)\/\".*b>(?<title>.+)<\/b><\/a/", $block, $matches);
			if ($matches AND count($matches["id"]))
			{
				$bFirst = true;
				$sMainTitle = null;
				foreach ($matches["id"] as $key => $categoryId)
				{
					// авто с другой ссылкой, единственная такая категория, думаю так сойдет
					$categories[] = [
						"okidoki_id" => ($categoryId == "auto" ? 16 : $categoryId),
						"title" => ($sMainTitle ? $sMainTitle . " > " : "") . $matches["title"][$key],
						"created_at" => date("Y-m-d H:i:s"),
						"updated_at" => date("Y-m-d H:i:s"),
					];

					if ($bFirst)
					{
						$bFirst = false;
						$sMainTitle = $matches["title"][$key];
					}
				}
			}
		}

		\DB::table("categories")->insert($categories);
		return redirect()->route('categories')->with("successMessage", "Категории успешно обновлены.");
	}
}
