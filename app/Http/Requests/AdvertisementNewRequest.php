<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdvertisementNewRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		// Не используем авторизацию пользователей
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'login' => 'required|advert_login',
			'category_id' => 'required|exists:categories,okidoki_id',
			'adtype' => 'required|in:1,2',
			'title_ee' => 'required',
			'title_ru' => 'required',

			// 'subtitle_ee' => '',
			// 'subtitle_ru' => '',

			'price_type' => 'required|in:1,2,3',
			'price' => 'numeric',
			'best_offer' => 'boolean',

			'tax' => 'required|in:0,1',

			'amount_sel' => 'required|in:0,1',
			'amount_val' => 'integer',

			'deliver_self' => 'boolean',
			'deliver_smartpost' => 'boolean',
			'deliver_post24' => 'boolean',
			'deliver_post' => 'boolean',
			'deliver_dpd' => 'boolean',
			'deliver_other' => 'boolean',

			'pay_okidoki' => 'boolean',
			'pay_cash' => 'boolean',
			'pay_bank' => 'boolean',
			'pay_seedetails' => 'boolean',

			'returns' => 'required|in:0,1,2',
			'returns_days' => 'integer|in:3,7,14,30,60',
			'returns_how' => 'in:1,2,3',
			'returns_who_pays' => 'in:1,2',
			// 'returns_comments' => '',

			// 'desc_ee' => '',
			// 'desc_ru' => '',
		];
	}

}
