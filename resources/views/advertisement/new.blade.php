@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			@include("particles.mainNav")

			<div class="panel panel-default">
				<div class="panel-heading">Изображения</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="control-label">Выбрать:</label>
						<input type="file" class="form-control" name="image" id="image">
					</div>

					<div id="images">
						@foreach($advert->images as $image)
						<div class="media"><div class="media-left" style="display: table-cell;"><img src="{{$image->getUrl()}}" alt="{{$image->title}}" style="width: 50px; height: 50px; margin-bottom: 0px;"></div><div class="media-body"><h4 class="media-heading">{{$image->title}}</h4><button class="btn btn-default btn-danger btn-xs js-delete" data-id="{{$image->getKey()}}">Удалить</button></div></div>
						@endforeach
					</div>
				</div>
				<div class="panel-footer">
					3 - бесплатно, последующие <b>0.50 €</b>.
				</div>
			</div>
		</div>

		<div class="col-lg-9">
			
			@include("particles.alertMessages")

			@if ($advert->getKey())
				<div class="panel panel-primary">
					<div class="panel-heading">{{ $advert->title_ru }} / {{ $advert->title_ee }}: {{ $advert->created_at }}</div>
				</div>
			@endif

			{!! Form::open() !!}

				<div class="panel panel-default">
					<div class="panel-heading">Пользователь</div>
					<div class="panel-body">
						<label class="control-label">От имени какого пользователя публиковать объявление:</label>
						{!! Form::select("login", $logins, old("login", $advert->login), ["class" => "form-control"]) !!}
						@if (!$logins)
							<div class="help-block" style="margin-bottom:0;"><span class="text-danger">Пользователь нет, добавьте аккаунты в файл конфигурации <code>config/okidoki.php</code></span></div>
						@endif
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Новое объявление</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-lg-6">
								<label class="control-label">Категория:</label>
								{!! Form::select("category_id", \App\Category::get()->lists("title", "okidoki_id"), old("category_id", $advert->category_id), ["class" => "form-control js-chosen"]) !!}
								@if (!\App\Category::count())
									<div class="help-block" style="margin-bottom:0;"><span class="text-danger">Список категорий пуст, <a href="{{route("categories")}}">перейдите на страницу</a> категорий и выполните первичный парсинг.</span></div>
								@endif
							</div>
							<div class="col-lg-6">
								<label class="control-label">Тип объявления:</label>
								{!! Form::select("adtype", [1 => "Предлагаю", 2 => "Ищу"], old("adtype", $advert->adtype), ["class" => "form-control js-chosen"]) !!}
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Заголовки</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label">Заголовок на <u>эстонском</u>:</label>
							<input type="text" class="form-control" name="title_ee" value="{{ old('title_ee', $advert->title_ee) }}">
						</div>
						
						<div class="form-group">
							<label class="control-label">Заголовок на <u>русском</u>:</label>
							<input type="text" class="form-control" name="title_ru" value="{{ old('title_ru', $advert->title_ru) }}">
						</div>

						<a href="javascript:void(0)" onclick="$('#subtitle').slideToggle(300);" class="btn btn-xs btn-default">Указать подзаголовки</a>

						<div id="subtitle" style="display:none;">
							<hr>

							<div class="form-group">
								<label class="control-label">Подзаголовок на <u>эстонском</u>:</label>
								<input type="text" class="form-control" name="subtitle_ee" value="{{ old('subtitle_ee', $advert->subtitle_ee) }}">
							</div>
							
							<div class="form-group">
								<label class="control-label">Подзаголовок на <u>русском</u>:</label>
								<input type="text" class="form-control" name="subtitle_ru" value="{{ old('subtitle_ru', $advert->subtitle_ru) }}">
							</div>

							<div class="help-block" style="margin-bottom:0;">* Подзаголовки платные (1.00 €)</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Цена</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label">Цена:</label>
							{!! Form::select("price_type", [1 => "Фиксированная", 2 => "Не определена", 3 => "Бесплатно"], old("price_type", $advert->price_type), ["class" => "form-control", "id" => "price_type"]) !!}
						</div>
						
						<div class="form-group" id="price_fix_block">
							<label class="control-label">Фиксированная цена:</label>
							<div class="input-group">
								<div class="input-group-addon">€</div>
								<input type="text" class="form-control" name="price" value="{{ old('price', $advert->price) }}">
							</div>
						</div>

						<div class="checkbox">
							<label>
								{!! Form::checkbox("best_offer", 1, old("best_offer", $advert->best_offer)) !!} Покупатели могут предлогать свою цену
							</label>
						</div>

						<div class="form-group">
							<label class="control-label">Цена содержит налог с оборота:</label>
							{!! Form::select("tax", [0 => "0% (частное или неналогообязанное лицо)", 1 => "20% (налогообязанное лицо)"], old("tax", $advert->tax), ["class" => "form-control"]) !!}
						</div>

						<div class="row">
							<div class="col-lg-9">
								<div class="form-group">
									<label class="control-label">Количество:</label>
									{!! Form::select("amount_sel", [0 => "Не указывать", 1 => "Указать"], old("amount_sel", $advert->amount_sel), ["class" => "form-control", "id" => "amount_sel"]) !!}
								</div>
							</div>
							<div class="col-lg-3">
								<div class="form-group" id="amount_val_block">
									<label class="control-label">Шт.:</label>
									<div class="input-group">
										<input type="text" class="form-control" name="amount_val" value="{{ old('amount_val', $advert->amount_val) }}">
										<div class="input-group-addon">шт.</div>
									</div>
								</div>
							</div>
						</div>

				
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">Как можно отправить товар?</div>
							<div class="panel-body">

								<div class="checkbox">
									<label>
										{!! Form::checkbox("deliver_self", 1, old("deliver_self", $advert->deliver_self)) !!} Самовывоз
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("deliver_smartpost", 1, old("deliver_smartpost", $advert->deliver_smartpost)) !!} SmartPOST
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("deliver_post24", 1, old("deliver_post24", $advert->deliver_post24)) !!} Omniva
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("deliver_post", 1, old("deliver_post", $advert->deliver_post)) !!} Почта
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("deliver_dpd", 1, old("deliver_dpd", $advert->deliver_dpd)) !!} DPD
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("deliver_other", 1, old("deliver_other", $advert->deliver_other)) !!} Другой способ доставки
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">Как можно оплатить товар?</div>
							<div class="panel-body">

								<div class="checkbox">
									<label>
										{!! Form::checkbox("pay_okidoki", 1, old("pay_okidoki", $advert->pay_okidoki)) !!} Через Депозит okidoki
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("pay_cash", 1, old("pay_cash", $advert->pay_cash)) !!} Наличными
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("pay_bank", 1, old("pay_bank", $advert->pay_bank)) !!} Банковским переводом
									</label>
								</div>
								<div class="checkbox">
									<label>
										{!! Form::checkbox("pay_seedetails", 1, old("pay_seedetails", $advert->pay_seedetails)) !!} Подробнее в описании
									</label>
								</div>

								<div class="form-group">
									<label class="control-label">Принимается ли возврат товара?</label>
									{!! Form::select("returns", [0 => "Не указывать", 1 => "Не принимается", 2 => "Принимается"], old("returns", $advert->returns), ["class" => "form-control", "id" =>"returns"]) !!}
								</div>

								<div id="returns_block" style="display:none;">
									<div class="form-group">
										<label class="control-label">В какой срок можно вернуть товар?</label>
										{!! Form::select("returns_days", [3 => "3 дня", 7 => "7 дней", 14 => "14 дней", 30 => "30 дней", 60 => "60 дней"], old("returns_days", $advert->returns_days), ["class" => "form-control"]) !!}
									</div>
									<div class="form-group">
										<label class="control-label">В случае возврата товара:</label>
										{!! Form::select("returns_how", [1 => "Возврат денег", 2 => "Замена на идентичный товар", 3 => "Замена на иной товар"], old("returns_how", $advert->returns_how), ["class" => "form-control"]) !!}
									</div>
									<div class="form-group">
										<label class="control-label">Кто оплачивает почтовые расходы?</label>
										{!! Form::select("returns_who_pays", [1 => "Покупатель", 2 => "Продавец"], old("returns_who_pays", $advert->returns_who_pays), ["class" => "form-control"]) !!}
									</div>
									<div class="form-group">
										<label class="control-label">Примечания:</label>
										{!! Form::textarea("returns_comments", old("returns_comments", $advert->returns_comments), ["class" => "form-control"]) !!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Описание</div>
					<div class="panel-body">

						<div role="tabpanel" id="desc_tab_panel_block">
							<ul class="nav nav-tabs" role="tablist" id="desc_tab_panel_block_navs">
								<li role="presentation" class="active"><a href="#desc_ee" aria-controls="desc_ee" role="tab" data-toggle="tab">На эстонском</a></li>
								<li role="presentation"><a href="#desc_ru" aria-controls="desc_ru" role="tab" data-toggle="tab">На русском</a></li>
							</ul>

							<div class="tab-content" style="margin-top:5px;">
								<div role="tabpanel" class="tab-pane active" id="desc_ee">
									{!! Form::textarea("desc_ee", old("desc_ee", $advert->desc_ee), ["class" => "form-control"]) !!}
								</div>
								<div role="tabpanel" class="tab-pane" id="desc_ru">
									{!! Form::textarea("desc_ru", old("desc_ru", $advert->desc_ru), ["class" => "form-control"]) !!}
								</div>
							</div>
						</div>

						<div class="help-block" style="margin-bottom:0;">Можно использовать все html-теги, которые позволены аккаунту на сайте okidoki.</div>
					</div>
				</div>

				<button type="submit" class="btn btn-primary">
					@if (!$advert->getKey())
					Создать объявление
					@else
					Обновить объявление
					@endif
				</button>

				<div class="help-block">Объявление будет добавлено в базу, после чего его можно будет опубликовать на сайте okidoki.</div>

				<div id="image_ids">
					@foreach($advert->images as $image)
						<input id="image_id_{{$image->getKey()}}" type="hidden" name="images[]" value="{{$image->getKey()}}">
					@endforeach
				</div>

			{!! Form::close() !!}
		</div>
	</div>
</div>

<script>
jQuery(function()
{
	var upload = new UploadImage($("#image"), {multiple:true});
	var imagesCont = $("#images");
	upload.on("add", function(el)
	{
		var mediaObject = new MediaObject();
		el.storage("mediaObject", mediaObject);
		el.storage("percent", $('<small/>').addClass('text-success').css("padding-left", "5px"));

		imagesCont.append(mediaObject.getTmpl());

		mediaObject.setHeadingText(el.getName());
		mediaObject.hideMediaLeft(1);
		mediaObject.appendToHeading(el.storage("percent"));
		mediaObject.getHeading();
	});

	upload.on("processAlways", function(el)
	{
		if (!el.getPreview())
		{
			return;
		}

		el.storage("mediaObject").showMediaLeft(300);
		el.storage("mediaObject").setMediaLeftHtml(el.getPreview());
	});

	upload.on("progress", function(el)
	{
		el.storage("percent").text(el.getPercent() + "%");
	});

	upload.on("fail", function(el)
	{
		el.storage("percent").text("Неудалось загрузить изображение").toggleClass('text-success text-danger');
	});

	upload.on("done", function(el)
	{
		if (!el.getRes().bOk)
		{
			el.storage("percent").text(el.getRes().message).toggleClass('text-success text-danger');
			return;
		}

		el.storage("mediaObject").setHeadingText(el.getRes().file.title);

		// create hiden input
		$('<input/>').prop("id", "image_id_"+el.getRes().file.id).prop("type", "hidden").prop("name", "images[]").val(el.getRes().file.id).appendTo($('#image_ids'));

		var imageObject = $("<img/>").prop("src", el.getRes().file.absUrl).prop("alt", el.getRes().file.title).css({
			width : 50,
			height: 50,
			marginBottom : 0
		});

		el.storage("mediaObject").setMediaLeftHtml(imageObject);

		var oBtnDelete = $('<button class="btn btn-default btn-danger btn-xs js-delete">Удалить</button>');
		oBtnDelete.data("id", el.getRes().file.id);
		el.storage("mediaObject").appendToBody(oBtnDelete);
	});

	imagesCont.on('click', '.js-delete', function(event) {
		event.preventDefault();

		$('#image_id_'+$(this).data("id")).remove();
		$(this).parent().parent().remove();
	});


	$('.js-chosen').chosen();

	var changeCallbacks = {
		price_type : function() {
			if ($(this).val() == 1) $('#price_fix_block').fadeTo(300, 1);
			else $('#price_fix_block').fadeTo(300, 0.3);
		},
		amount_sel : function() {
			if ($(this).val() == 1) $('#amount_val_block').fadeTo(300, 1);
			else $('#amount_val_block').fadeTo(300, 0.3);
		},
		returns : function() {
			if ($(this).val() == 2) $('#returns_block').show();
			else $('#returns_block').hide();
		}
	};

	$('#price_type').change(changeCallbacks.price_type);
	changeCallbacks.price_type.call($('#price_type'));

	$('#amount_sel').change(changeCallbacks.amount_sel);
	changeCallbacks.amount_sel.call($('#amount_sel'));

	$('#returns').change(changeCallbacks.returns);
	changeCallbacks.returns.call($('#returns'));

	$('#desc_tab_panel_block_navs a').click(function (e)
	{
		e.preventDefault()
		$(this).tab('show')
	});
});
</script>
@endsection