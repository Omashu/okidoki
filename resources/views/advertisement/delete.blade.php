@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			@include("particles.mainNav")
		</div>

		<div class="col-lg-9">
			@include("particles.alertMessages")

			{!! Form::open() !!}
			<div class="panel panel-primary" id="sure_block">
				<div class="panel-heading">Удаление {{ $advert->title_ru }}</div>
				<div class="panel-body">
					<b>{{ $advert->title_ru }}</b> будет удалено из базы. На сайте okidoki объявление не будет удалено.
					Продолжить?
				</div>
				<div class="panel-footer">
					<button type="submit" class="btn btn-success">Продолжить</button>
					<a href="{{ route("advertisement") }}" class="btn btn-danger">Отмена</a>
				</div>
			</div>

			{!! Form::close() !!}

		</div>
	</div>
</div>


@endsection
