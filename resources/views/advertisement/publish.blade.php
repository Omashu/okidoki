@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			@include("particles.mainNav")
		</div>

		<div class="col-lg-9">
			@include("particles.alertMessages")

			<div class="panel panel-primary" id="sure_block">
				<div class="panel-heading">{{ $advert->title_ru }}</div>
				<div class="panel-body">
					<b>{{ $advert->title_ru }}</b> будет опубликовано на сайт okidoki. Начать?
				</div>
				<div class="panel-footer">
					<button type="button" class="btn btn-success" id="submit">Начать публикацию</button>
					<a href="{{ route("advertisement") }}" class="btn btn-danger">Отмена</a>
				</div>
			</div>

			<div id="results"></div>

		</div>
	</div>
</div>

<script>
var Exec = new Publisher({{$advert->getKey()}});
Exec.on("before", function()
{
	$('#sure_block').hide();
	$('#submit').prop("disabled", true).addClass("disabled");
});

jQuery(function()
{
	Exec.addResultsBlock($('#results'));

	$('#submit').click(function()
	{
		Exec.exec();
	});
});
</script>
@endsection
