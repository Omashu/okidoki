@if (session('errorMessage'))
	<div class="alert alert-danger">
		{{ session('errorMessage') }}
	</div>
@endif

@if (session('successMessage'))
	<div class="alert alert-success">
		{{ session('successMessage') }}
	</div>
@endif

@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul style="list-style:none;margin:0;padding:0;">
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif