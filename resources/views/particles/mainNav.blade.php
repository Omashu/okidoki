<div class="panel panel-default">
	<div class="panel-heading">Навигация</div>

	<div class="panel-body">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="{{ route("advertisement") }}">Все объявления <span class="badge pull-right">{{ App\Advert::count() }}</span></a></li>
			<li><a href="{{ route("advertisementNew") }}">Новое объявление</a></li>
			<li><a href="{{ route("categories") }}">Категории объявлений</a></li>
		</ul>
	</div>
</div>