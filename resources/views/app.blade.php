<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Okidoki</title>

	<script>
	var AjaxAdvertisementPublishUrl = "{{route('ajaxAdvertisementPublish')}}";
	var AjaxUploadImage = "{{route('ajaxUploadImage')}}";
	var _Token = "{{ csrf_token() }}";
	</script>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Chosen -->
	<link href='{{ asset('chosen/chosen.min.css') }}' rel='stylesheet' type='text/css'>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('chosen/chosen.jquery.min.js') }}"/></script>
	<script src="{{ asset('js/Publisher.js') }}"/></script>
	<script src="{{ asset('js/UploadImage.js') }}"/></script>
	<script src="{{ asset('js/MediaObject.js') }}"/></script>

	<!-- Fileupload -->
	<script src="{{ asset('jQuery-File-Upload/js/vendor/jquery.ui.widget.js') }}"/></script>
	<script src="{{ asset('JavaScript-Load-Image/js/load-image.all.min.js') }}"/></script>
	<script src="{{ asset('JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js') }}"/></script>
	<script src="{{ asset('jQuery-File-Upload/js/jquery.iframe-transport.js') }}"/></script>
	<script src="{{ asset('jQuery-File-Upload/js/jquery.fileupload.js') }}"/></script>
	<script src="{{ asset('jQuery-File-Upload/js/jquery.fileupload-process.js') }}"/></script>
	<script src="{{ asset('jQuery-File-Upload/js/jquery.fileupload-image.js') }}"/></script>
	<script src="{{ asset('jQuery-File-Upload/js/jquery.fileupload-validate.js') }}"/></script>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">Oki</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/') }}">Home</a></li>
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="help-block">Version: {{ env("APP_VERSION") }} &nbsp;&nbsp;&nbsp; <b>Skype: mivl.fl</b></div>
			</div>
		</div>
	</div>

<script>
jQuery(function()
{
	$('[data-toggle="tooltip"]').tooltip({placement:"auto"});
});
</script>

</body>
</html>
