@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			
			<div class="panel panel-warning">
				<div class="panel-body">
					
					@if (!App\Category::count())
						<div class="help-block" style="margin-top:0;">
							Категорий нет, выполните парсинг для получения списка всех категорий.
						</div>
						<a href="{{ route("categoriesSync") }}" class="btn btn-success" style="display:block;">Построить категории</a>
					@else
						<a href="{{ route("categoriesSync") }}" class="btn btn-success" style="display:block;">Перестроить категории</a>
					@endif
				</div>
			</div>

			@include("particles.mainNav")
		</div>

		<div class="col-lg-9">
			@include("particles.alertMessages")

			<div class="panel panel-primary">
				<div class="panel-heading">Категории объявлений с сайта okidoki <span class="badge pull-right">{{ App\Category::count() }}</span></div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>#Okidoki Id</th>
								<th>Название</th>
							</tr>
						</thead>
						<tbody>
							@foreach($categories as $category)
							<tr>
								<td>{{ $category->getKey() }}</td>
								<td>{{ $category->okidoki_id }}</td>
								<td>{{ $category->title }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
