@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-3">
			@include("particles.mainNav")
		</div>

		<div class="col-lg-9">
			@include("particles.alertMessages")

			<div class="panel panel-primary">
				<div class="panel-heading">Список объявлений <span class="badge pull-right">{{ App\Advert::count() }}</span></div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>#Okidoki Id</th>
								<th>Название</th>
								<th>Дата публикации</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($adverts as $advert)
							<tr>
								<td>{{ $advert->getKey() }}</td>
								<td>{!! $advert->okidoki_id ?: '<code>NULL</code>' !!}</td>
								<td>{!! $advert->title_ru ?: '<span class="text-muted">RU название не указано...</span>' !!}</td>
								<td>{!! $advert->published_at ? $advert->published_at : '<span class="text-danger">Не опубликовано</span>' !!}</td>
								<td align="center">
									@if ($advert->published_at)
										<a href="{{ route("advertisementCloneAndEdit", $advert) }}" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Будет создано новое объявление по шаблону этого.">Использовать</a>
										<a href="{{route("advertisementCloneAndPublish", $advert)}}" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Объявление будет клонировано и опубликовано повторно.">Опубликовать еще раз</a>
									@else
										<a href="{{ route("advertisementEdit", $advert)}}" class="btn btn-default btn-xs" data-toggle="tooltip" title="Можно отредактировать.">Изменить</a>
										<a href="{{route("advertisementPublish",$advert)}}" class="btn btn-success btn-xs" data-toggle="tooltip" title="Можно опубликовать на okidoki.">Опубликовать</a>
									@endif

									<a href="{{route("advertisementDelete",$advert)}}" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Удалить объявление из базы.">Удалить</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
