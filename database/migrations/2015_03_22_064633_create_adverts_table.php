<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("adverts", function(Blueprint $table)
		{
			$table->increments('id');

			// пользователь okidoki от имени которого опубликуем
			$table->string("login");

			// если null, значит не опубликовано
			$table->integer("okidoki_id")->nullable()->default(null);

			// выбранная категория для публикации
			$table->integer("category_id")->nullable()->default(null);

			// тип объявления, 1 = предлагаю, 2 = ищу
			$table->enum('adtype', [1, 2])->default(1);

			// заголовки, эстонский/русский
			$table->string("title_ee", 60);
			$table->string("title_ru", 60);

			// подзаголовки
			$table->string("subtitle_ee", 60);
			$table->string("subtitle_ru", 60);

			// тип цены, 1 = фикс., 2 = не определена, 3 = бесплатно
			$table->enum('price_type', [1, 2, 3])->default(1);
			// цена (актуально для типа 1)
			$table->float("price")->default(0.00);
			// покупатели могут предлагать свою цену (1 = да, 0 = нет)
			$table->boolean("best_offer")->default(true);

			// налог в цене (0 частник, 1 20% налог в цене товара)
			$table->enum('tax', [0, 1])->default(0);

			// кол-во товара (0 указать, 1 не указать)
			$table->enum('amount_sel', [0, 1])->default(1);
			$table->integer('amount_val')->default(1);

			// способы доставки
			$table->boolean("deliver_self")->default(false);
			$table->boolean("deliver_smartpost")->default(false);
			$table->boolean("deliver_post24")->default(false);
			$table->boolean("deliver_post")->default(false);
			$table->boolean("deliver_dpd")->default(false);
			$table->boolean("deliver_other")->default(false);

			// способы оплаты
			$table->boolean("pay_okidoki")->default(false);
			$table->boolean("pay_cash")->default(false);
			$table->boolean("pay_bank")->default(false);
			$table->boolean("pay_seedetails")->default(false);

			// возможен ли возврат, 0 не указывать, 1 нет, 2 возврат возможен
			$table->enum('returns', [0, 1, 2])->default(0);
			$table->integer("returns_days")->default(14);
			$table->enum("returns_how", [1, 2, 3])->default(1);
			$table->enum("returns_who_pays", [1, 2])->default(1);
			$table->text("returns_comments");

			// с местонахождением товара позже, чет там мудрости

			// описание
			$table->text("desc_ee");
			$table->text("desc_ru");

			$table->dateTime('published_at')->nullable()->default(null);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("adverts");
	}

}
